# [2.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.1.0...v2.2.0) (2024-07-17)


### Features

* performance optimization ([8ed2859](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/8ed2859bf6e905b27bb91078770a5c0ab70d6c54))

# [2.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.0.6...v2.1.0) (2024-06-04)


### Features

* reformated and added blueprint + bug fix + optimalization ([9c5b1d7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/9c5b1d7412a4fd34da5c02c214cfa07d88d7ec8a))

## [2.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.0.5...v2.0.6) (2024-05-23)


### Bug Fixes

* **deps:** update dependency @fortawesome/fontawesome-free to v6.5.2 ([ba2b0f1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/ba2b0f15ac3b1af6b0fac19c90681c8cdfa45cbd))
* **deps:** update dependency autoprefixer to v10.4.19 ([dca0117](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/dca0117c62f57a2e753f038dc5f5dedf0f9ba674))

## [2.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.0.4...v2.0.5) (2024-04-08)


### Bug Fixes

* save_acceptances will correctly response 'fail' when accept_aups request does not exist ([65b1c6a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/65b1c6afef39b92912e133b7dcb20bec580691c0))

## [2.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.0.3...v2.0.4) (2024-01-26)


### Bug Fixes

* **deps:** pin dependency @fortawesome/fontawesome-free to 6.5.1 ([5283840](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/52838403bb4e9c2920f9c8df35fa36129b3dbef1))

## [2.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.0.2...v2.0.3) (2024-01-26)


### Bug Fixes

* openapi gitlab integration ([78afd82](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/78afd8290c72b999d08e03c4d372908ce96d1f50))

## [2.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.0.1...v2.0.2) (2023-11-20)


### Bug Fixes

* allow pymongo v3 ([8f048b9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/8f048b95f10df731659ac3e6cde88fcabdc35905))

## [2.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v2.0.0...v2.0.1) (2023-02-06)


### Bug Fixes

* include templates, static and open-api-specification.yaml when installed with pip ([7589c5e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/7589c5e40bcb25956d07197e59f4b820cb242cc5))

# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v1.4.1...v2.0.0) (2023-02-03)


### Features

* app renamed to aup_manager, npm i and npm run compile_sass are running during installation ([34e4eb5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/34e4eb5203410fe59dedf91ad9be37ee5f1fb986))


### BREAKING CHANGES

* app renamed to aup_manager

## [1.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v1.4.0...v1.4.1) (2023-02-03)


### Bug Fixes

* publish to pypi.org ([305b4ad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/305b4ad49f1ef8b361d26edaa802f5f9fd2ead06))

# [1.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v1.3.0...v1.4.0) (2022-12-14)


### Features

* admin gui implemented ([a9ae7e0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/a9ae7e0cca62f3a7ed843cf26d81a47b1a59fd45))
* page allowing users to accept aups ([9a333c1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/9a333c111500e864c9470cdce89a8f83e305e1f6))

# [1.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v1.2.0...v1.3.0) (2022-10-27)


### Features

* implemented login functionality, added base for templates and bootstrap ([62b99bf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/62b99bfcac6e3f000a5544692ec3c4a44377532b))

# [1.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v1.1.0...v1.2.0) (2022-10-20)


### Features

* added hierarchy, openapi_specification, connexion + swaggerui, skeleton for gui methods/pages ([0fca5a3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/0fca5a36de9a86365e12009ae9efc0dafd51f44c))

# [1.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/compare/v1.0.0...v1.1.0) (2022-10-13)


### Features

* perunConnector implemented ([8e3abc5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/8e3abc5b017369dcb981288fdac9649d7880ea6b))

# 1.0.0 (2022-10-04)


### Features

* connectorInterface DatabaseInterface MongodbConnector MongodbDatabase and models implemented ([47cf606](https://gitlab.ics.muni.cz/perun/perun-proxyidp/aup-manager/commit/47cf606e041a417e5fe857543e8d225d42131080))
